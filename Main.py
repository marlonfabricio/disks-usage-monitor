from DiskUtility import DiskUtility
from Mailing import Mailing
import ConfigParser
import ast


def main():
    config = ConfigParser.ConfigParser()
    config.readfp(open('config.cfg'))

    # Getting configuration from config.cfg file
    server_name = config.get('Server', 'server_name')
    file_types = ast.literal_eval(config.get('Server', 'file_types'))
    max_usage = config.getfloat('Server', 'max_usage')
    mailgun_domain = config.get('Mailgun', 'mailgun_domain')
    mailgun_key = config.get('Mailgun', 'mailgun_key')
    sender = config.get('Mailgun', 'sender')
    to = ast.literal_eval(config.get('Mailgun', 'to'))
    subject = config.get('Mailgun', 'subject')

    # Instance class to verify disk usage
    du = DiskUtility(file_types, max_usage)
    disk_usage = du.verify_disk_usage()

    if (disk_usage != None):
        # If some disk exceeds max usage param, then we send mail notification to email lists
        mailing = Mailing(mailgun_domain, mailgun_key, max_usage, server_name)
        mailing.send_mailgun_email({
            "sender": sender,
            "to": to,
            "subject": subject,
            "content": str(disk_usage),
        })


if __name__ == "__main__":
    main()
