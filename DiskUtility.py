import psutil


class DiskUtility:
    def __init__(self, file_types, max_usage):
        self.file_types = file_types
        self.max_usage = max_usage

    def verify_disk_usage(self):
        exceeds = False
        partitions = []
        for p in psutil.disk_partitions():
            if (p.fstype.lower() not in self.file_types):
                continue

            usage = psutil.disk_usage(p.mountpoint).percent
            partitions.append({
                "device": p.device,
                "mountpoint": p.mountpoint,
                "fstype": p.fstype,
                "usage": usage,
            })

            if (usage > self.max_usage):
                exceeds = True

        return partitions if exceeds else None
