import codecs
import requests


class Mailing:
    def __init__(self, domain, key, percent, server_name):
        self.domain = domain
        self.key = key
        self.percent = percent
        self.server_name = server_name

    def send_mailgun_email(self, mailing_data):
        file = codecs.open('mail-content.html', 'r', 'utf-8')
        html = str(file.read())
        html = html.replace('{ content }', mailing_data["content"])
        html = html.replace('{ percentage }', str(self.percent))
        html = html.replace('{ server_name }', self.server_name)

        return requests.post(
            "https://api.mailgun.net/v3/domain/messages".replace('domain', self.domain),
            auth=("api", self.key),
            data={
                "from": mailing_data["sender"],
                "to": mailing_data["to"],
                "subject": mailing_data["subject"],
                "html": html,
            }
        )
